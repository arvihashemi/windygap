$(document).ready(function(){
    var onSuccessCheckIn = function(position) {
        $("#latitude").val(position.coords.latitude);
        $("#longitude").val(position.coords.longitude);
        $("#altitude").val(position.coords.altitude);
        $("#accuracy").val(position.coords.accuracy);
        $("#altitudeAccuracy").val(position.coords.altitudeAccuracy);
        $("#heading").val(position.coords.heading);
        $("#speed").val(position.coords.speed);
        $("#timestamp").val(position.timestamp);

        // Google maps
        //moveToLocation(position.coords.latitude, position.coords.longitude);
        //addMarker({lat: position.coords.latitude, lng: position.coords.longitude});
        MapCheckIn.map_openlayers_move_to_location(position.coords.latitude, position.coords.longitude);
        MapCheckIn.map_openlayers_add_marker(position.coords.latitude, position.coords.longitude);
        MapCheckIn.map_openlayers_add_popup(position.coords.latitude, position.coords.longitude);
        MapCheckIn.map_openlayers_ajax_checkin(position.coords);
        
    };

    var onSuccessMoveMap = function(position) {
        $("#latitude").val(position.coords.latitude);
        $("#longitude").val(position.coords.longitude);
        $("#altitude").val(position.coords.altitude);
        $("#accuracy").val(position.coords.accuracy);
        $("#altitudeAccuracy").val(position.coords.altitudeAccuracy);
        $("#heading").val(position.coords.heading);
        $("#speed").val(position.coords.speed);
        $("#timestamp").val(position.timestamp);

        // Google maps
        //moveToLocation(position.coords.latitude, position.coords.longitude);
        //addMarker({lat: position.coords.latitude, lng: position.coords.longitude});
        MapCheckIn.map_openlayers_move_to_location(position.coords.latitude, position.coords.longitude);
    };
    
    function onError(error) {
        alert("Code: "    + error.code    + "\n" +    "Message: " + error.message + "\n");

        $("#getCurrentLocation").button("enable");
        $("#watchGeolocation").button("enable");
        $("#stopWatchingGeolocation").button("disable");

    }

    function getOptions() {
        var enableHighAccuracy = ($("#highAccuracy").val() === "true");
        var maximumAge = parseInt($("#maximumAge").val());
        var timeout = parseInt($("#geolocationTimeout").val());

        return {
            maximumAge: maximumAge,
            timeout: timeout,
            enableHighAccuracy: enableHighAccuracy
        }
    }

    $(document).on("click", "#getCurrentLocation", function() {
        navigator.geolocation.getCurrentPosition(onSuccessCheckIn, onError, getOptions());
    });
    
    // run at document.ready()
    navigator.geolocation.getCurrentPosition(onSuccessMoveMap, onError, getOptions());
    
    var watchId;

    $(document).on("click", "#watchGeolocation", function() {
        $("#getCurrentLocation").button("disable");
        $("#watchGeolocation").button("disable");
        $("#stopWatchingGeolocation").button("enable");

        watchId = navigator.geolocation.watchPosition(onSuccessMoveMap, onError, getOptions());
    });

    $(document).on("click", "#stopWatchingGeolocation", function() {
        $("#getCurrentLocation").button("enable");
        $("#watchGeolocation").button("enable");
        $("#stopWatchingGeolocation").button("disable");

        navigator.geolocation.clearWatch(watchId);
    });
});